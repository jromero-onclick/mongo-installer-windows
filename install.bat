ECHO "MONGODB installation"
PAUSE
ECHO "MongoDB install"
CALL msiexec.exe /i "%~dp0mongodb-win32-x86_64-2008plus-ssl-3.0.6-signed.msi" INSTALLLOCATION="C:\PMPBUDGETING\MongoDB" ADDLOCAL="all" /quiet /L*v "%~dp0install_results.txt"
ECHO Returncode: %ERRORLEVEL%
ECHO "Configuring MongoDB"
XCOPY "%~dp0install_files\MONGO_DATA" "C:\PMPBUDGETING" /T /E /I /Y
ECHO F| COPY "%~dp0install_files\mongod.cfg" "C:\PMPBUDGETING\MongoDB\mongod.cfg"
ECHO "Creating MongoDB Service"
CALL sc.exe create MongoDB binPath= "C:\PMPBUDGETING\MongoDB\bin\mongod.exe --service --config=\"C:\PMPBUDGETING\MongoDB\mongod.cfg\"" DisplayName= "Service MongoDB" start= "auto"
PAUSE